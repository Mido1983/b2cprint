<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once __DIR__.'/vendor/autoload.php';
use App\Models\Person;


/*
 * for my test only
 * include_once 'Models/Person.php';
 */



/**
 * Insert User data for class Women
 */
$userW = new \App\Models\Women();
$userW->setUserProp('age', 'twenty');
$userW->setUserProp('name', 'Maria');

/**
 * Print User data for class Women
 */
echo $userW->getUserProp('age'); // Print Error
echo $userW->getUserProp('name');
echo $userW->getUserProp('sex');

/**
 * Insert User data for class Men
 */
$userM = new \App\Models\Men();
$userM->setUserProp('age', 25);
$userM->setUserProp('name', 'Asaf');
$userM->setUserProp('sex', 'Women');

/**
 * Print User data for class Men
 */
echo $userM->getUserProp('age');
echo $userM->getUserProp('name');
echo $userM->getUserProp('sex'); // Print Error
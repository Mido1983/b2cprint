<?php
namespace App\Models;

trait userData {
  private $propArray = ['name', 'age', 'sex'];
  public function setUserProp($prop,$data)
  {
    /*
     * Validation
     */
    try {
      if( !in_array($prop, $this->propArray))
        throw new Exception('Bad property type.');
      if($prop == 'sex')
        throw new Exception('You can\'t change this property.');
      if($prop == 'age' && !is_numeric($data))
        throw new Exception('Age property need number.');
      $this->$prop = $data;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
  }
  
  public function getUserProp($prop)
  {
    /*
   * Validation
   */
    try {
      if( !in_array($prop, $this->propArray))
        throw new Exception('Bad property type.');
      if( empty($this->$prop))
        throw new Exception('Empty data.');
      return $this->$prop;
      
    } catch (Exception $e) {
        echo $e->getMessage();
    }
  }
}
  
  
/**
 * Class Person
 */
class Person {
  use userData;
  protected $name;
  protected $age;
  protected $sex;
  function  __construct($sex = FALSE){
    $this->sex = $sex;
  }
}

/**
 * Class Men
 */
class Men extends Person {
  function  __construct(){
    parent::__construct('male');
  }
}

/**
 * Class Women
 */
class Women extends Person {
  function  __construct()  {
    parent ::__construct('female');
  }
}

